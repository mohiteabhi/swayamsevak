package com.swk.firebase_connection;


import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import org.json.JSONObject;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import com.swk.DashBoard.Swk_LoginPage;
import com.swk.controller.LoginController;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class FirebaseService {
    private TextField emailField; 
    private PasswordField passwordField; 
    private LoginController loginController; 
    private Swk_LoginPage swk_LoginPage; 
    

    public FirebaseService(Swk_LoginPage swk_LoginPage, TextField emailField, PasswordField passwordField){
        this.swk_LoginPage = swk_LoginPage; 
        this.emailField = emailField; 
        this.passwordField = passwordField; 
    }
    public boolean signUp(){
        String email = emailField.getText(); 
        String password = passwordField.getText(); 

        System.out.println("222222222");
        try{
            UserRecord.CreateRequest request = new UserRecord.CreateRequest()
                .setEmail(email)
                .setPassword(password)
                .setDisabled(false); 
            UserRecord userRecord = FirebaseAuth.getInstance(null).createUser(request); 
            System.out.println("Succesfully created user: "+ userRecord.getUid());
            showAlert("Success","User created succesfully. "); 
            return true; 
        } catch(FirebaseAuthException e){
            e.printStackTrace();
            showAlert("Error","Failed to create user "+ e.getMessage()); 
            return false ;
        }
    }

    public boolean login(){
        String email = emailField.getText(); 
        String password = passwordField.getText(); 

        // System.out.println(11111);

        try{
            String apiKey = "AIzaSyChiQACN-onssMtG5qxmqcRiSZfSRTOoic"; 

            URL url = new URL("https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyChiQACN-onssMtG5qxmqcRiSZfSRTOoic"); 
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8"); ///////////
            conn.setDoOutput(true);

            JSONObject jsonRequest = new JSONObject(); 
            jsonRequest.put("email", email); 
            jsonRequest.put("password",password); 
            jsonRequest.put("returnSecureToken", true); 

            try(OutputStream os = conn.getOutputStream()){
                byte[] input = jsonRequest.toString().getBytes(StandardCharsets.UTF_8);
                os.write(input, 0,input.length);
            }

            if (conn.getResponseCode() == 200){
                showAlert(true); 
                return true; 
            }else{
                showAlert("Invalid Login", "Invalid credentials !!!"); 
                return false; 
            }
        }catch(Exception e){
            e.printStackTrace();
            showAlert(false);
            return false; 
        }
    }

        private void showAlert(String title, String message){
            Alert alert = new Alert(Alert.AlertType.INFORMATION); 
            alert.setTitle(title);
            alert.setHeaderText(null); 
            alert.setContentText(message);
            alert.showAndWait(); 
        }

        private void showAlert(boolean isLoggedIn){
            Label msg = new Label("welcome");
            msg.setAlignment(Pos.CENTER);

            Button logoutButton = new Button("LOGOUT");

            VBox vBox = new VBox(100, msg,logoutButton);
            
            logoutButton.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                   loginController.initializationLoginScene(); 
                }
                
            });

            Scene scene = new Scene(vBox,400,200); 
            // swk_LoginPage.setPrimaryStageScene(scene);
        }
    }
