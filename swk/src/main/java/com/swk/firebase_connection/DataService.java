package com.swk.firebase_connection;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.cloud.FirestoreClient;
import com.swk.DashBoard.Swk_AfterLoginPage;
import com.swk.controller.navigationApp;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;

import com.google.firebase.FirebaseOptions;

public class DataService {

    private static Firestore db;
    private navigationApp app; 

    static {
        try {
            initializeFirebase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static void initializeFirebase() throws IOException {
        FileInputStream serviceAccount = new FileInputStream("swk\\src\\main\\resources\\eventsadd.json");

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://swayamsevak-7eb39-default-rtdb.asia-southeast1.firebasedatabase.app")
                .build();

        FirebaseApp.initializeApp(options);

        db = FirestoreClient.getFirestore();

    }

    public void addData(String collection, String document, Map<String, Object> data)
            throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection(collection).document(document);
        ApiFuture<WriteResult> result = docRef.set(data);
        result.get();
    }

    public void getData(String collection, String document) throws ExecutionException, InterruptedException {
        try {
            DocumentReference docRef = db.collection(collection).document(document);
            ApiFuture<DocumentSnapshot> future = docRef.get();
            DocumentSnapshot docSnap = future.get();

            if (docSnap.exists()) {
                String city = docSnap.getString("city");
                System.out.println(city);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

   public VBox getData(String collection) throws ExecutionException, InterruptedException {
    // Create SVGPath for the orange wave shape
    SVGPath orangeWave = new SVGPath();
    orangeWave.setContent("M0,152 Q300,102 600,152 Q900,202 1200,152 Q1500,102 1800,152 Q1950,177 2100,152 L2100,0 L0,0 Z");
    orangeWave.setFill(Color.rgb(248, 164, 4)); // Set fill color for the wave shape
    orangeWave.setStroke(Color.TRANSPARENT); // Optional: remove stroke if not needed
    orangeWave.setStrokeWidth(0); // Optional: set stroke width

    // Create StackPane for the wave at the top
    StackPane waveContainer = new StackPane();
    waveContainer.getChildren().add(orangeWave);

    // Create the main VBox
    VBox vb = new VBox(20);
    vb.setStyle("-fx-background-color: lightblue;"); // Set background color for the outer VBox
    vb.setPadding(new Insets(0, 0, 20, 0)); // Add padding to the bottom

    try {
        ApiFuture<QuerySnapshot> future = db.collection(collection).get();
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();

        for (QueryDocumentSnapshot document : documents) {
            VBox itemBox = new VBox(10);
            itemBox.setPadding(new Insets(15));
            itemBox.setStyle(
                    "-fx-background-color: white;" + // Set initial background color for the inner VBox
                    "-fx-border-color: Black;" + // Set initial border color
                    "-fx-border-width: 2;" + // Set border width
                    "-fx-border-radius: 5;" // Set border radius
            );
            itemBox.setMaxWidth(1000); // Set the maximum width for the event VBox
            

            HBox cityBox = new HBox(10);
            cityBox.getChildren().addAll(new Label("City: "), new Label(document.getString("city")));

            HBox eventBox = new HBox(10);
            eventBox.getChildren().addAll(new Label("Event: "), new Label(document.getString("event")));

            HBox dateBox = new HBox(10);
            dateBox.getChildren().addAll(new Label("Date: "), new Label(document.getString("date")));

            HBox locationBox = new HBox(10);
            locationBox.getChildren().addAll(new Label("Location: "), new Label(document.getString("venue")));

            HBox timeBox = new HBox(10);
            timeBox.getChildren().addAll(new Label("Time: "), new Label(document.getString("time")));

            HBox volunteersBox = new HBox(10);
            AtomicInteger totalVolunteers = new AtomicInteger(Integer.parseInt(document.getString("volunteers")));

            volunteersBox.getChildren().addAll(
                    new Label("Volunteers Required: "),
                    new Label(String.valueOf(totalVolunteers)));

            HBox buttonBox = new HBox(10);
            Button joinButton = new Button("Join");
            joinButton.setStyle("-fx-background-color: green; -fx-text-fill: white;");

            joinButton.setOnAction(e -> {
                int currentVolunteers = totalVolunteers.decrementAndGet();

                System.out.println(currentVolunteers);

                if (currentVolunteers < 0) {
                    totalVolunteers.incrementAndGet(); // Rollback
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Volunteer Limit Reached");
                    alert.setHeaderText(null);
                    alert.setContentText("Volunteers are full!");
                    alert.showAndWait();
                } else {
                    // Update the Firestore document with the new number of volunteers
                    DocumentReference docRef = document.getReference();
                    try {
                        docRef.update("volunteers", String.valueOf(currentVolunteers)).get();

                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("Event Joined");
                        alert.setHeaderText(null);
                        // Add a GIF to the Alert
                        Image gif = new Image("file:swayamsevak\\swk\\src\\main\\resources\\GIF\\verified.gif");
                        ImageView gifView = new ImageView(gif);
                        gifView.setFitWidth(100); // Set the desired width
                        gifView.setFitHeight(100); // Set the desired height
                        gifView.setPreserveRatio(true); // Preserve the aspect ratio
                        alert.setGraphic(gifView);
                        alert.setContentText("Event joined successfully!");
                        alert.showAndWait();

                        // Check if volunteers are full
                        if (currentVolunteers <= 0) {
                            joinButton.setDisable(true);
                            joinButton.setStyle("-fx-background-color: grey; -fx-text-fill: white;");

                            itemBox.setStyle(
                                    "-fx-background-color: grey;" + // Change background color to grey
                                    "-fx-border-color: grey;" + // Change border color to grey
                                    "-fx-border-width: 2;" + // Set border width
                                    "-fx-border-radius: 5;" // Set border radius
                            );
                        }

                    } catch (InterruptedException | ExecutionException ex) {
                        ex.printStackTrace();
                        Alert alert = new Alert(AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText(null);
                        alert.setContentText("Failed to join event. Please try again.");
                        alert.showAndWait();
                    }
                }
            });

            buttonBox.getChildren().add(joinButton);

            itemBox.getChildren().addAll(cityBox, eventBox, dateBox, locationBox, timeBox, volunteersBox, buttonBox);
            vb.getChildren().add(itemBox);
        }
    } catch (Exception e) {
        e.printStackTrace();
    }

    // Add back button outside the VBox
    Button backButton = new Button("Back");
    backButton.setStyle("-fx-background-color: grey; -fx-text-fill: white;");
    backButton.setOnAction(e -> {
        
        navigationApp.getInstance().jumpToAfterLoginPage();
    });
    backButton.setAlignment(Pos.BOTTOM_RIGHT);

    VBox mainBox = new VBox();
    mainBox.getChildren().addAll(waveContainer, vb, backButton); // Add wave, content, and back button

    return mainBox;
}
    
    
    

    public List<QueryDocumentSnapshot> getALLEvents() throws ExecutionException, InterruptedException {
        ApiFuture<QuerySnapshot> query = db.collection("events").get();
        QuerySnapshot querySnapshot = query.get();
        return querySnapshot.getDocuments();
    }
}
