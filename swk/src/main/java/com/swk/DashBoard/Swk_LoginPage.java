package com.swk.DashBoard;

import com.swk.controller.navigationApp;
import com.swk.firebase_connection.FirebaseService;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Swk_LoginPage {

    private navigationApp app;
    private StackPane view;
    private FirebaseService firebaseService;

    public Swk_LoginPage(navigationApp app) {
        this.app = app;
        initialize();
    }

    private void initialize() {

        //Shadow effect
        DropShadow shadow = new DropShadow();
        shadow.setColor(Color.rgb(255, 139, 0));
        shadow.setRadius(20);
        shadow.setSpread(0.2);

        // Load the logo image and convert it to button
        Image swk_logoImage = new Image("file:swk\\src\\main\\resources\\Images\\logo\\logo1.png");
        ImageView swk_logoImageView = new ImageView(swk_logoImage);
        swk_logoImageView.setFitWidth(500);
        swk_logoImageView.setFitHeight(500);
        swk_logoImageView.setOpacity(0.4);
        Button swk_logoButton = new Button();
        swk_logoButton.setGraphic(swk_logoImageView);
        swk_logoButton.setOnMouseEntered(e -> swk_logoButton.setEffect(shadow));
        swk_logoButton.setOnMouseExited(e -> swk_logoButton.setEffect(null));
        swk_logoButton.setCursor(Cursor.HAND);
        swk_logoButton.setStyle(
                "-fx-background-color: transparent; " +
                        "-fx-padding: 50; " +
                        "-fx-border-width: 0; " +
                        "-fx-border-color: transparent;");

        //AnchorPane for positioning of logo
        AnchorPane logoAnchor = new AnchorPane();
        logoAnchor.getChildren().add(swk_logoButton);
        AnchorPane.setTopAnchor(swk_logoButton, -200.0);

        // Left side of Scene
        VBox swk_mainLoginLayout = new VBox(25);
        swk_mainLoginLayout.setPadding(new Insets(50, 30, 50, 30)); // Adjusted padding for top and bottom
        swk_mainLoginLayout.setMaxWidth(500);
        swk_mainLoginLayout.setMaxHeight(900);
        swk_mainLoginLayout.setAlignment(Pos.CENTER_LEFT);
        swk_mainLoginLayout.setStyle(
                "-fx-background-color: rgba(205, 209, 228, 0.5); -fx-border-color: #31210D;-fx-border-width: 1; -fx-border-radius: 10; -fx-background-radius: 10;");

        //Login Title in login layout
        Label swk_welcomeText = new Label("Login");
        // swk_welcomeText.setTextFill(Color.VIOLET);
        swk_welcomeText.setFont(Font.font("Arial", FontWeight.BOLD, 30));

        //Hyperlink for signup
        Hyperlink signUpLink = new Hyperlink("Sign Up");
        signUpLink.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                app.jumpToSignup();
            }
        });

        //HBox for signUp text and hyyperlink
        HBox signUpBox = new HBox(5);
        signUpBox.setAlignment(Pos.CENTER_LEFT);
        signUpBox.getChildren().addAll(new Label("Doesn't have an account yet?"), signUpLink);

        //VBox for Email Address Title and Field
        VBox swk_emailLayout = new VBox(5);
        swk_emailLayout.setAlignment(Pos.CENTER_LEFT);

        Label swk_emailLabel = new Label("Email Address");
        swk_emailLabel.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        TextField swk_emailField = new TextField();
        swk_emailField.setMinHeight(40); //TextField Height
        swk_emailField.setPromptText("you@example.com"); //Prompt Text
        //EmailField styling
        swk_emailField.setStyle(
                "-fx-prompt-text-fill: derive(-fx-control-inner-background, -60%); -fx-background-color: null; -fx-border-color: gray; -fx-border-width: 2; -fx-border-radius: 10");
        
        swk_emailLayout.getChildren().addAll(swk_emailLabel, swk_emailField); //Added emailLabel and emailField in swk_emailLayout VBox

        //VBox for Password Layout
        VBox swk_passwordLayout = new VBox(5); 
        swk_passwordLayout.setAlignment(Pos.CENTER_LEFT);

        Label swk_passwordLabel = new Label("Password"); //Passward Label
        swk_passwordLabel.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        PasswordField passwordField = new PasswordField(); //Passward Field
        passwordField.setMinHeight(40);
        passwordField.setPromptText("Enter 6 character or more");
        passwordField.setStyle(
                "-fx-prompt-text-fill: derive(-fx-control-inner-background, -60%); -fx-background-color: null; -fx-border-color: gray; -fx-border-width: 2; -fx-border-radius: 10");
        swk_passwordLayout.getChildren().addAll(swk_passwordLabel, passwordField);

        firebaseService = new FirebaseService(this, swk_emailField, passwordField);  //Firebase service initialized

        // Login Button
        Button swk_loginButton = new Button("LOGIN");
        swk_loginButton.setFont(Font.font("Arial", FontWeight.BOLD, 22));
        swk_loginButton.setStyle(
                "-fx-background-color: rgba(255, 116, 0);-fx-border-color: yellow; -fx-text-fill: white; -fx-border-radius: 5;-fx-background-radius: 5;");
        swk_loginButton.setMinHeight(40);
        swk_loginButton.setMinWidth(150);

        //Event Handler for login button
        swk_loginButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                firebaseService.login();
                app.jumpToAfterLoginPage();
            }
        });

        //Action Event after pointing mouse on login button
        swk_loginButton.setOnMouseEntered(e -> swk_loginButton.setStyle(
        "-fx-background-color: rgba(196, 89, 0); -fx-border-color: yellow; -fx-text-fill: white; -fx-border-radius: 5; -fx-background-radius: 5;"));
        swk_loginButton.setOnMouseExited(e -> swk_loginButton.setStyle(
        "-fx-background-color: rgba(255, 116, 0); -fx-border-color: yellow; -fx-text-fill: white; -fx-border-radius: 5; -fx-background-radius: 5;"));

        //Back Button
        Button back = new Button("Back");
        back.setFont(Font.font("Arial", FontWeight.BOLD, 18));
        back.setStyle(
                "-fx-background-color: rgba(255, 42, 0); -fx-text-fill: white; -fx-border-color: red; -fx-border-radius: 5;-fx-background-radius: 5;");
        back.setMinHeight(40);
        back.setMinWidth(50);

        //Event Handler for back button
        back.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                app.jumpToWelcome();
            }
        });

        //Action Event after pointing mouse on back button
        back.setOnMouseEntered(e -> back.setStyle("-fx-background-color: rgba(182, 30, 0); -fx-text-fill: white; -fx-border-color: red; -fx-border-radius: 5;-fx-background-radius: 5;"));
        back.setOnMouseExited(e -> back.setStyle("-fx-background-color: rgba(255, 42, 0); -fx-text-fill: white; -fx-border-color: red; -fx-border-radius: 5;-fx-background-radius: 5;"));

        //HBox for holding loginButton, back button
        HBox buttonLayout = new HBox(300); // spacing between buttons
        buttonLayout.setAlignment(Pos.CENTER_LEFT);
        buttonLayout.getChildren().addAll(swk_loginButton, back);

        swk_mainLoginLayout.getChildren().addAll(
                logoAnchor,
                swk_welcomeText,
                signUpBox,
                swk_emailLayout,
                swk_passwordLayout,
                buttonLayout);

        // Right side image
        Image image = new Image("file:swk\\src\\main\\resources\\Images\\wel2.png");
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(1000);
        imageView.setFitWidth(800);
        imageView.setStyle("-fx-border-radius: 100; -fx-background-radius: 100;");

        // Create a HBox for left and right side
        HBox mainLayout = new HBox(50);
        mainLayout.setAlignment(Pos.CENTER);
        mainLayout.setPadding(new Insets(20));
        mainLayout.getChildren().addAll(swk_mainLoginLayout, imageView);

        // Set the background image
        Image backgroundImage = new Image("file:swk\\src\\main\\resources\\Images\\bg4.jpg");
        ImageView backgroundImageView = new ImageView(backgroundImage);
        backgroundImageView.setFitWidth(1920);
        backgroundImageView.setFitHeight(1080); 
        backgroundImageView.setPreserveRatio(true);
        backgroundImageView.setEffect(new GaussianBlur(30)); //Applied blur effect to background Image

        //Adding backgroundImageView and mainLayout in StackPane
        view = new StackPane();
        view.getChildren().addAll(backgroundImageView, mainLayout);

        // Apply hover effects
        //addHoverEffect(swk_emailField);  //Added Hover effect to email Field
        //addHoverEffect(passwordField); //Added Hover effect to passward  Field
    }
    
    /*private void addHoverEffect(TextField textField) {
        textField.setOnMouseEntered(e -> textField.setStyle(
                "-fx-background-color: null; -fx-border-color: white; -fx-border-width: 2; -fx-border-radius: 10;"));
        textField.setOnMouseExited(e -> textField.setStyle(
                "-fx-background-color: null; -fx-border-color: gray; -fx-border-radius: 10;"));
    }

    private void addHoverEffect(PasswordField passwordField) {
        passwordField.setOnMouseEntered(e -> passwordField.setStyle(
                "-fx-background-color: null; -fx-border-color: gray; -fx-border-width: 2; -fx-border-radius: 10;"));
        passwordField.setOnMouseExited(e -> passwordField.setStyle(
                "-fx-background-color: null; -fx-border-color: white; -fx-border-radius: 10;"));
    }*/

    public StackPane getView() {
        return view;
    }
    
}
//@author: Abhijeet Mohite
//Updated on: 25/07/2024
















































