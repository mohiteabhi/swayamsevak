package com.swk.DashBoard;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.swk.controller.navigationApp;
import com.swk.firebase_connection.DataService;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Swk_AddEvent {
    private navigationApp app;
    private GridPane grid;
    private Group root; 
    private DataService dataservice;
    

    public Swk_AddEvent(navigationApp app) {
        this.app = app;
        dataservice = new DataService(); // Initialize DataService
        initialize();
    }

    private void initialize() {

         root = new Group(); // Create the root group

        // Create SVGPath for the orange wave shape
        SVGPath orangeWave = new SVGPath();
        orangeWave.setContent("M0,152 Q300,102 600,152 Q900,202 1200,152 Q1500,102 1800,152 Q1950,177 2100,152 L2100,0 L0,0 Z");
        orangeWave.setFill(Color.rgb(248, 164, 4)); // Set fill color for the wave shape
        root.getChildren().add(orangeWave); // Add wave shape to the root group

        VBox swk_AddEventLayout = new VBox(25);
        swk_AddEventLayout.setPadding(new Insets(50, 30, 50, 30)); // Adjusted padding for top and bottom
        swk_AddEventLayout.setMaxWidth(700);
        swk_AddEventLayout.setMaxHeight(900);
        swk_AddEventLayout.setAlignment(Pos.CENTER_LEFT);
        swk_AddEventLayout.setStyle(
                "-fx-background-color: rgba(205, 209, 228, 0.5); -fx-border-color: #31210D;-fx-border-width: 1; -fx-border-radius: 10; -fx-background-radius: 10;");

        // Second Image
        Image img2 = new Image("file:swayamsevak\\swk\\src\\main\\resources\\Images\\logo\\logo1.png");
        ImageView imageView2 = new ImageView(img2);
        imageView2.setLayoutX(0); // Set X position of the image
        imageView2.setLayoutY(0); // Set Y position of the image
        imageView2.setFitWidth(300); // Adjust width of the image
        imageView2.setFitHeight(180); // Adjust height of the image
        root.getChildren().add(imageView2); // Add image to the root group

        // Create buttons
        HBox hbox = new HBox(10); // Create HBox with spacing between buttons
        hbox.setPadding(new Insets(25)); // Set padding around the HBox
        hbox.setAlignment(Pos.TOP_RIGHT); // Align HBox to the top right corner

        // Create three buttons
        Button homeButton = new Button("Home");
        homeButton.setStyle("-fx-background-color: #F8A404; -fx-text-fill: white; -fx-font-size: 24px; -fx-background-radius: 20px;");
        homeButton.setPadding(new Insets(10, 20, 10, 20)); // Set padding for the button

        Button AboutButton = new Button("About us");
        AboutButton.setStyle("-fx-background-color: #F8A404; -fx-text-fill: white; -fx-font-size: 24px; -fx-background-radius: 20px;");
        AboutButton.setPadding(new Insets(10, 20, 10, 20)); // Set padding for the button
        AboutButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                app.jumpToAboutUs();
            }
        });

        Button loginButton = new Button("Log In");
        loginButton.setStyle("-fx-background-color: #F8A404; -fx-text-fill: white; -fx-font-size: 24px; -fx-background-radius: 20px;");
        loginButton.setPadding(new Insets(10, 20, 10, 20)); // Set padding for the button
        loginButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                app.jumpToLogin();
            }
        });

        // Add buttons to the HBox
        hbox.getChildren().addAll(homeButton, AboutButton, loginButton);

        // Position the HBox in the top right corner
        hbox.setLayoutX(1300); // Set X position of the HBox
        hbox.setLayoutY(20); // Set Y position of the HBox

        // Add HBox to the root group
        root.getChildren().add(hbox);

         // Create the GridPane
        grid = new GridPane();
        grid.setPadding(new Insets(70)); // Adjust padding for the GridPane
        grid.setVgap(30);
        grid.setHgap(8);


        // Event City Name Label and TextField
        Label cityLabel = new Label("Event City Name:");
        cityLabel.setFont(new Font("Arial", 30));
        GridPane.setConstraints(cityLabel, 0, 0);
        TextField cityInput = new TextField();
        cityInput.setPrefSize(400, 50);
        cityInput.setStyle("-fx-font-size: 25px;-fx-border-color: black");
        GridPane.setConstraints(cityInput, 1, 0);

        // Event Description Label and TextField
        Label eventLabel = new Label("About Event:");
        eventLabel.setFont(new Font("Arial", 30));
        GridPane.setConstraints(eventLabel, 0, 1);
        TextField eventInput = new TextField();
        eventInput.setPrefSize(400, 50);
        eventInput.setStyle("-fx-font-size: 25px; -fx-border-color: black");
        GridPane.setConstraints(eventInput, 1, 1);

        // Event Date Label and DatePicker
        Label dateLabel = new Label("Event Date:");
        dateLabel.setFont(new Font("Arial", 30));
        GridPane.setConstraints(dateLabel, 0, 2);
        DatePicker datePicker = new DatePicker();
        datePicker.setPrefSize(400, 50);
        datePicker.setStyle("-fx-font-size: 25px;-fx-border-color: black");
        GridPane.setConstraints(datePicker, 1, 2);

        // Venue Label and TextField
        Label venueLabel = new Label("Venue:");
        venueLabel.setFont(new Font("Arial", 30));
        GridPane.setConstraints(venueLabel, 0, 3);
        TextField venueInput = new TextField();
        venueInput.setPrefSize(400, 50);
        venueInput.setStyle("-fx-font-size: 25px;-fx-border-color: black");
        GridPane.setConstraints(venueInput, 1, 3);

        // Time Label and TextField
        Label timeLabel = new Label("Time:");
        timeLabel.setFont(new Font("Arial", 30));
        GridPane.setConstraints(timeLabel, 0, 4);
        TextField timeInput = new TextField();
        timeInput.setPrefSize(400, 50);
        timeInput.setStyle("-fx-font-size: 25px;-fx-border-color: black");
        GridPane.setConstraints(timeInput, 1, 4);

        // Volunteers Label and TextField
        Label volunteersLabel = new Label("Volunteers Required:");
        volunteersLabel.setFont(new Font("Arial", 30));
        GridPane.setConstraints(volunteersLabel, 0, 5);
        TextField volunteersInput = new TextField();
        volunteersInput.setPrefSize(400, 50);
        volunteersInput.setStyle("-fx-font-size: 25px;-fx-border-color: black");
        GridPane.setConstraints(volunteersInput, 1, 5);

        Label ContactNo = new Label("Mobile No:");
        ContactNo.setFont(new Font("Arial", 30));
        GridPane.setConstraints(ContactNo, 0, 6);
        TextField ContactInput = new TextField();
        ContactInput.setPrefSize(400, 50);
        ContactInput.setStyle("-fx-font-size: 25px;-fx-border-color: black");
        GridPane.setConstraints(ContactInput, 1, 6);

        // Add Event Button
        Button addButton = new Button("Add Event");
        addButton.setStyle("-fx-background-color: #F8A404; -fx-text-fill: white; -fx-font-size: 25px; -fx-background-radius: 20px;");
        addButton.setPadding(new Insets(10, 20, 10, 20)); // Set padding for the button
        addButton.setLayoutX(100); // Set X position of the button
        addButton.setLayoutY(700);
        addButton.setOnAction(e -> {
            String eventDetails = "City: " + cityInput.getText() +
                                  "Event: " + eventInput.getText() +
                                  "Date: " + datePicker.getValue() +
                                  "Venue: " + venueInput.getText() +
                                  "Time: " + timeInput.getText() +
                                  "Volunteers Required: " + volunteersInput.getText() +
                                  "Contact No: "+ ContactInput.getText();
            System.out.println(eventDetails);

            // Store event data in Firebase Realtime Database
            handleEvent(cityInput.getText(), eventInput.getText(), datePicker.getValue().toString(),
                        venueInput.getText(), timeInput.getText(), volunteersInput.getText(), ContactInput.getText());

            // Clear form fields after submission
            cityInput.clear();
            eventInput.clear();
            datePicker.setValue(null);
            venueInput.clear();
            timeInput.clear();
            volunteersInput.clear();
            ContactInput.clear();
        });

        Button backButton = new Button("Back");
        backButton.setStyle("-fx-background-color: rgba(255, 42, 0);; -fx-text-fill: white; -fx-font-size: 25px; -fx-background-radius: 20px;");
        backButton.setPadding(new Insets(10, 20, 10, 20)); // Set padding for the button
        backButton.setLayoutX(100); // Set X position of the button
        backButton.setLayoutY(700);
        backButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                app.jumpToAfterLoginPage();
            }
        });

        HBox hb = new HBox(10,addButton, backButton);
        GridPane.setConstraints(hb, 1, 7);


        // Add all the controls to the grid
        grid.getChildren().addAll(cityLabel, cityInput, eventLabel, eventInput, dateLabel, datePicker, venueLabel,
                venueInput, timeLabel, timeInput, volunteersLabel, volunteersInput, hb);
    }

    public VBox createEventsVBox() {
        VBox root = new VBox();
        root.setPadding(new Insets(10));

        try {
            List<QueryDocumentSnapshot> documents = dataservice.getALLEvents();
            for (QueryDocumentSnapshot document : documents) {
                String city = document.getString("city");
                String event = document.getString("event");
                String date = document.getString("date");
                String venue = document.getString("venue");
                String time = document.getString("time");
                String volunteers = document.getString("volunteers");

                String eventDetails = "City: " + city + "Event: " + event + "Date: " + date + "Venue: " + venue
                        + "Time: " + time + "Volunteers Required: " + volunteers;

                root.getChildren().add(createNewsItem(eventDetails));
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            // Handle exceptions as needed
        }

        return root;
    }

    private VBox createNewsItem(String eventDetails) {
        VBox newsItem = new VBox();
        newsItem.setPadding(new Insets(10));
        newsItem.setSpacing(10);
        newsItem.setStyle(
                "-fx-background-color: rgba(255, 255, 255, 0.2); -fx-background-radius: 10px; -fx-border-color: #ccc; -fx-border-radius: 10px; -fx-border-width: 1px;");

        Label eventDetailsLabel = new Label(eventDetails);
        eventDetailsLabel.setStyle("-fx-font-weight: bold;-fx-font-size: 14px;");
        eventDetailsLabel.setTextAlignment(TextAlignment.LEFT);

        newsItem.getChildren().add(eventDetailsLabel);
        return newsItem;
    }

    private void handleEvent(String city, String event, String date, String venue, String time, String volunteers, String ContactNo) {
        try {
            // Create a data map
            Map<String, Object> data = new HashMap<>();
            data.put("city", city);
            data.put("event", event);
            data.put("date", date);
            data.put("venue", venue);
            data.put("time", time);
            data.put("volunteers", volunteers);
            data.put("Contact", ContactNo);

            dataservice.addData(city, event, data);
            // Example: Store data in Firebase Realtime Database
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("events");
            ref.push().setValueAsync(data); // Push data to a new node under "events"

            // Example: Store data in Firestore
            // dataservice.addData("collection", "document", data); // Use your DataService to add data to Firestore

        } catch (Exception e) {
            e.printStackTrace();
            // Handle exception as needed
        }
    }

    public GridPane getView() {
        return grid;
    }
}
