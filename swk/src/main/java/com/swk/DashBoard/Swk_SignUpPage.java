package com.swk.DashBoard;

import com.swk.controller.navigationApp;
import com.swk.firebase_connection.FirebaseService;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Swk_SignUpPage {

        private navigationApp app;
        private StackPane view;
        private FirebaseService firebaseService;

        public Swk_SignUpPage(navigationApp app) {
                this.app = app;
                initialize();
        }

        private void initialize() {
                DropShadow shadow = new DropShadow();
                shadow.setColor(Color.rgb(255, 139, 0));
                shadow.setRadius(20);
                shadow.setSpread(0.2);

                // Load the logo image and convert it to button
                Image swk_logosignupImage = new Image("file:swk\\src\\main\\resources\\Images\\logo\\logo1.png");
                ImageView swk_logosignupImageView = new ImageView(swk_logosignupImage);
                swk_logosignupImageView.setFitWidth(500);
                swk_logosignupImageView.setFitHeight(500);
                swk_logosignupImageView.setOpacity(0.4);
                Button swk_logosignupButton = new Button();
                swk_logosignupButton.setGraphic(swk_logosignupImageView);
                swk_logosignupButton.setOnMouseEntered(e -> swk_logosignupButton.setEffect(shadow));
                swk_logosignupButton.setOnMouseExited(e -> swk_logosignupButton.setEffect(null));
                swk_logosignupButton.setCursor(Cursor.HAND);
                swk_logosignupButton.setStyle(
                                "-fx-background-color: transparent; " +
                                                "-fx-padding: 50; " +
                                                "-fx-border-width: 0; " +
                                                "-fx-border-color: transparent;");

                // AnchorPane for positioning of logo
                AnchorPane signuplogoAnchor = new AnchorPane();
                signuplogoAnchor.getChildren().add(swk_logosignupButton);
                AnchorPane.setTopAnchor(swk_logosignupButton, -200.0);

                VBox swk_mainSignUpLayout = new VBox(25);
                swk_mainSignUpLayout.setPadding(new Insets(50, 30, 50, 30)); // Adjusted padding for top and bottom
                swk_mainSignUpLayout.setMaxWidth(500);
                swk_mainSignUpLayout.setMaxHeight(900);
                swk_mainSignUpLayout.setAlignment(Pos.CENTER_LEFT);
                swk_mainSignUpLayout.setStyle(
                                "-fx-background-color: rgba(205, 209, 228, 0.5); -fx-border-color: #31210D;-fx-border-width: 1; -fx-border-radius: 10; -fx-background-radius: 10;");

                Label swk_signUpText = new Label("Sign Up");
                // swk_welcomeText.setTextFill(Color.VIOLET);
                swk_signUpText.setFont(Font.font("Arial", FontWeight.BOLD, 30));
                                
                VBox firstNameLayout = new VBox(5);
                firstNameLayout.setAlignment(Pos.CENTER_LEFT);

                Label firstName = new Label("First Name");
                firstName.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
                TextField firstNameField = new TextField();
                firstNameField.setMinHeight(40); // TextField Height
                // EmailField styling
                firstNameField.setStyle(
                "-fx-prompt-text-fill: derive(-fx-control-inner-background, 100%);-fx-background-color: null; -fx-border-width:2; -fx-border-color: gray;-fx-border-radius: 10");

                firstNameLayout.getChildren().addAll(firstName, firstNameField);

                VBox lastNameLayout = new VBox(5);
                lastNameLayout.setAlignment(Pos.CENTER_LEFT);

                Label lastName = new Label("Last Name");
                lastName.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
                TextField lastNameField = new TextField();
                lastNameField.setMinHeight(40); // TextField Height
                // EmailField styling
                lastNameField.setStyle(
                "-fx-prompt-text-fill: derive(-fx-control-inner-background, 100%);-fx-background-color: null; -fx-border-width:2; -fx-border-color: gray;-fx-border-radius: 10");

                lastNameLayout.getChildren().addAll(lastName, lastNameField);

                VBox swk_emailLayout = new VBox(5);
                swk_emailLayout.setAlignment(Pos.CENTER_LEFT);

                Label swk_emailLabel = new Label("Email Address");
                swk_emailLabel.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
                TextField swk_emailField = new TextField();
                swk_emailField.setMinHeight(40); // TextField Height
                swk_emailField.setPromptText("you@example.com"); // Prompt Text
                // EmailField styling
                swk_emailField.setStyle(
                                "-fx-prompt-text-fill: derive(-fx-control-inner-background, -60%); -fx-background-color: null; -fx-border-width:2; -fx-border-color: gray; -fx-border-radius: 10");

                swk_emailLayout.getChildren().addAll(swk_emailLabel, swk_emailField);

                VBox swk_passwordLayout = new VBox(5);
                swk_passwordLayout.setAlignment(Pos.CENTER_LEFT);

                Label swk_passwordLabel = new Label("Password"); // Passward Label
                swk_passwordLabel.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
                PasswordField passwordField = new PasswordField(); // Passward Field
                passwordField.setMinHeight(40);
                passwordField.setPromptText("Enter 6 character or more");
                passwordField.setStyle(
                                "-fx-prompt-text-fill: derive(-fx-control-inner-background, -60%);-fx-background-color: null; -fx-border-width:2; -fx-border-color: gray; -fx-border-radius: 10");
                swk_passwordLayout.getChildren().addAll(swk_passwordLabel, passwordField);

                Button swk_signUpButton = new Button("Sign Up");
                swk_signUpButton.setFont(Font.font("Arial", FontWeight.BOLD, 22));
                swk_signUpButton.setStyle(
                                "-fx-background-color: rgba(255, 116, 0);-fx-border-color: yellow; -fx-text-fill: white; -fx-border-radius: 5;-fx-background-radius: 5;");
                swk_signUpButton.setMinHeight(50);
                swk_signUpButton.setMinWidth(150);

                // Event Handler for login button
                swk_signUpButton.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {

                                firebaseService.signUp(); 
                                Alert alert = new Alert(AlertType.INFORMATION);
                                alert.setTitle("Account Created");
                                alert.setHeaderText(null);
                                alert.setContentText("Account created successfully!!!");
                                alert.showAndWait();
                        }
                });

                // Action Event after pointing mouse on signup button
                swk_signUpButton.setOnMouseEntered(e -> swk_signUpButton.setStyle(
                                "-fx-background-color: rgba(196, 89, 0); -fx-border-color: yellow; -fx-text-fill: white; -fx-border-radius: 5; -fx-background-radius: 5;"));
                swk_signUpButton.setOnMouseExited(e -> swk_signUpButton.setStyle(
                                "-fx-background-color: rgba(255, 116, 0); -fx-border-color: yellow; -fx-text-fill: white; -fx-border-radius: 5; -fx-background-radius: 5;"));

                // Back Button
                Button back = new Button("Back");
                back.setFont(Font.font("Arial", FontWeight.BOLD, 18));
                back.setStyle(
                                "-fx-background-color: rgba(255, 42, 0); -fx-text-fill: white; -fx-border-color: red; -fx-border-radius: 5;-fx-background-radius: 5;");
                back.setMinHeight(40);
                back.setMinWidth(50);

                // Event Handler for back button
                back.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                                app.jumpToLogin();
                        }
                });

                // Action Event after pointing mouse on back button
                back.setOnMouseEntered(e -> back.setStyle(
                                "-fx-background-color: rgba(182, 30, 0); -fx-text-fill: white; -fx-border-color: red; -fx-border-radius: 5;-fx-background-radius: 5;"));
                back.setOnMouseExited(e -> back.setStyle(
                                "-fx-background-color: rgba(255, 42, 0); -fx-text-fill: white; -fx-border-color: red; -fx-border-radius: 5;-fx-background-radius: 5;"));

                // HBox for holding SignUpButton, back button
                HBox buttonLayout = new HBox(380); // spacing between buttons
                buttonLayout.setAlignment(Pos.CENTER_LEFT);
                buttonLayout.getChildren().addAll(swk_signUpButton, back);

                swk_mainSignUpLayout.getChildren().addAll(signuplogoAnchor, swk_signUpText, firstNameLayout, lastNameLayout, swk_emailLayout, swk_passwordLayout,
                                buttonLayout);
                // Set the background image
                Image backgroundsignImage = new Image("file:swk\\src\\main\\resources\\Images\\bg4.jpg");
                ImageView backgroundsignImageView = new ImageView(backgroundsignImage);
                backgroundsignImageView.setFitWidth(1920);
                backgroundsignImageView.setFitHeight(1080);
                backgroundsignImageView.setPreserveRatio(true);
                backgroundsignImageView.setEffect(new GaussianBlur(30));
                // Create a StackPane and add the VBox to it
                view = new StackPane();
                view.setAlignment(Pos.CENTER); // Center the VBox within the StackPane
                view.getChildren().addAll(backgroundsignImageView, swk_mainSignUpLayout);
        }

        public StackPane getView() {
                return view;
        }
}
