package com.swk.DashBoard;

import com.swk.controller.LoginController;
import com.swk.controller.navigationApp;
import com.swk.firebase_connection.DataService;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Swk_AfterLoginPage {

    private navigationApp app;
    private StackPane view;;
    private Swk_EventResult result;
    private Group root;

    private DataService dataService = new DataService();

    public Swk_AfterLoginPage(navigationApp app) {
        this.app = app;
        initialize();
    }

    private void initialize() {

        DropShadow shadow = new DropShadow();
        shadow.setColor(javafx.scene.paint.Color.YELLOW);
        shadow.setRadius(20);
        shadow.setSpread(0.2);

        root = new Group(); // Create the root group

        // Create SVGPath for the orange wave shape
        SVGPath orangeWave = new SVGPath();
        orangeWave.setContent(
                "M0,152 Q300,102 600,152 Q900,202 1200,152 Q1500,102 1800,152 Q1950,177 2100,152 L2100,0 L0,0 Z");
        orangeWave.setFill(Color.rgb(248, 164, 4)); // Set fill color for the wave shape
        root.getChildren().add(orangeWave); // Add wave shape to the root group

        // Second Image
        Image img2 = new Image("file:swayamsevak\\swk\\src\\main\\resources\\Images\\logo\\logo1.png");
        ImageView imageView2 = new ImageView(img2);
        imageView2.setLayoutX(0); // Set X position of the image
        imageView2.setLayoutY(0); // Set Y position of the image
        imageView2.setFitWidth(300); // Adjust width of the image
        imageView2.setFitHeight(180); // Adjust height of the image
        root.getChildren().add(imageView2); // Add image to the root group

        // Create buttons
        HBox hbox = new HBox(10); // Create HBox with spacing between buttons
        hbox.setPadding(new Insets(25)); // Set padding around the HBox
        hbox.setAlignment(Pos.TOP_RIGHT); // Align HBox to the top right corner

        // Create three buttons
        Button homeButton = new Button("Home");
        homeButton.setStyle(
                "-fx-background-color: #F8A404; -fx-text-fill: white; -fx-font-size: 24px; -fx-background-radius: 20px;");
        homeButton.setPadding(new Insets(10, 20, 10, 20)); // Set padding for the button
        homeButton.setOnMouseEntered(e -> homeButton.setEffect(shadow));
        homeButton.setOnMouseExited(e -> homeButton.setEffect(null));

        Button AboutButton = new Button("About us");
        AboutButton.setStyle(
                "-fx-background-color: #F8A404; -fx-text-fill: white; -fx-font-size: 24px; -fx-background-radius: 20px;");
        AboutButton.setPadding(new Insets(10, 20, 10, 20)); // Set padding for the button
        AboutButton.setOnMouseEntered(e -> AboutButton.setEffect(shadow));
        AboutButton.setOnMouseExited(e -> AboutButton.setEffect(null));
        AboutButton.setOnAction(event -> app.jumpToAboutUs());

        Button loginButton = new Button("Log Out");
        loginButton.setStyle(
                "-fx-background-color: #F8A404; -fx-text-fill: white; -fx-font-size: 24px; -fx-background-radius: 20px;");
        loginButton.setPadding(new Insets(10, 20, 10, 20)); // Set padding for the button
        loginButton.setOnMouseEntered(e -> loginButton.setEffect(shadow));
        loginButton.setOnMouseExited(e -> loginButton.setEffect(null));

        loginButton.setOnAction(event -> app.jumpToLogin());

        // Add buttons to the HBox
        hbox.getChildren().addAll(homeButton, AboutButton, loginButton);

        // Position the HBox in the top right corner
        hbox.setLayoutX(1300); // Set X position of the HBox
        hbox.setLayoutY(20); // Set Y position of the HBox

        // Add HBox to the root group
        root.getChildren().add(hbox);

        // The search bar and search button
        TextField searchBar = new TextField();
        searchBar.setPromptText("Search By City");
        searchBar.setStyle("-fx-font-size: 25pt; -fx-background-color: rgba(255, 255, 255, 0.8); -fx-border-color:black; -fx-border-radius: 10;");


        searchBar.setPrefSize(800, 40);

        Button searchButton = new Button("Search");

        searchButton.setStyle(
                "-fx-background-color: #F8A404; -fx-text-fill: white; -fx-font-size: 25pt; -fx-background-radius: 20px;");
        searchButton.setPrefSize(300, 40);
        searchButton.setOnMouseEntered(e -> searchButton.setEffect(shadow));
        searchButton.setOnMouseExited(e -> searchButton.setEffect(null));

        searchButton.setOnAction(new EventHandler<ActionEvent>() {
            Swk_EventResult eventResult = new Swk_EventResult();
            VBox vb = eventResult.createEventVBox();

            @Override
            public void handle(ActionEvent event) {
                try {
                    vb = dataService.getData(searchBar.getText());
                    vb.setStyle("-fx-background-color: lightblue;");

                    Scene sc = new Scene(vb);
                    app.setScene(sc);
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });


        HBox searchBox = new HBox(1,searchBar,searchButton);
        searchBox.setLayoutX(400);
        searchBox.setLayoutY(300);
        root.getChildren().addAll(searchBox); 

        
        Button registerButton = new Button("Register Event");
        registerButton.setStyle("-fx-background-color: #206842; -fx-text-fill: white; -fx-font-size: 30px; -fx-background-radius: 20px;");
        registerButton.setPadding(new Insets(10, 20, 10, 20)); // Set padding for the button
        registerButton.setLayoutX(850);
        registerButton.setLayoutY(400);

        registerButton.setOnAction(new EventHandler<ActionEvent>() {
    
        @Override
        public void handle(ActionEvent event) {
        app.jumpToAddEvent();
        }
    
        });

        root.getChildren().addAll(registerButton);
        

        // Image
        Image img = new Image("file:swk\\src\\main\\resources\\Images\\AfterLoginPage\\minimalistic_line_art.png");
        ImageView imageView = new ImageView(img);
        imageView.setFitWidth(1200); // Adjust width of the image
        imageView.setFitHeight(600); // Adjust height of the image
        imageView.setX(380); // Set X position of the image
        imageView.setY(400); // Set Y position of the image
        root.getChildren().add(imageView); // Add image to the root group

    }


    public Group getView() {
        return root;
    }

}
