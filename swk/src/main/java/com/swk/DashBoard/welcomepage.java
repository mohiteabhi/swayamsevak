package com.swk.DashBoard;

import com.swk.controller.navigationApp;
import javafx.animation.FadeTransition;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class welcomepage {

    private navigationApp app; // Reference to the NavigationApp instance

    // Constructor to initialize the landingPage with a NavigationApp instance
    public welcomepage(navigationApp app) {
        this.app = app;
        initialize();
    }

    private Group root; // Root group for the scene graph

    // Method to initialize the landing page components
    public void initialize() {

        DropShadow shadow = new DropShadow();
        shadow.setColor(javafx.scene.paint.Color.YELLOW);
        shadow.setRadius(20);
        shadow.setSpread(0.2);

        root = new Group(); // Create the root group

        // Create SVGPath for the orange wave shape
        SVGPath orangeWave = new SVGPath();
        orangeWave.setContent("M0,152 Q300,102 600,152 Q900,202 1200,152 Q1500,102 1800,152 Q1950,177 2100,152 L2100,0 L0,0 Z");
        orangeWave.setFill(Color.rgb(248, 164, 4)); // Set fill color for the wave shape
        root.getChildren().add(orangeWave); // Add wave shape to the root group

        // Text "Why do we use it?"
        Label mainText = new Label("Why do we use it?");
        mainText.setFont(new Font("Arial", 60)); // Set font style and size
        mainText.setTextFill(Color.rgb(89, 97, 170)); // Set text color
        mainText.setLayoutX(100); // Set X position of the text
        mainText.setLayoutY(380); // Set Y position of the text
        root.getChildren().add(mainText); // Add text to the root group        
        // Create a FadeTransition
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(500), mainText);              
        fadeTransition.setFromValue(1.0); // Start opacity value
        fadeTransition.setToValue(0.0); // End opacity value
        fadeTransition.setCycleCount(FadeTransition.INDEFINITE); // Infinite number of cycles
        fadeTransition.setAutoReverse(true); // Reverse direction on alternating cycles.

        
        // Start the fade transition
        fadeTransition.play();


        // Description text
        Label descriptionText = new Label("We use an app for volunteers to communicate with event organizers to streamline communication, coordinate tasks, manage schedules, and ensure efficient event execution.");
        descriptionText.setFont(new Font("Arial", 30)); // Set font style and size
        descriptionText.setTextFill(Color.rgb(53, 64, 84)); // Set text color
        descriptionText.setWrapText(true); // Enable text wrapping
        descriptionText.setTextAlignment(TextAlignment.LEFT); // Set text alignment
        descriptionText.setPrefWidth(500); // Set preferred width for the text
        descriptionText.setLayoutX(100); // Set X position of the text
        descriptionText.setLayoutY(480); // Set Y position of the text


        

        root.getChildren().add(descriptionText); // Add text to the root group

        // Create a fade transition for the descriptionText


        // Get started button
        Button getStartedButton = new Button("Get started");
        getStartedButton.setStyle("-fx-background-color: #F8A404; -fx-text-fill: white; -fx-font-size: 30px; -fx-background-radius: 20px;");
        getStartedButton.setPadding(new Insets(10, 20, 10, 20)); // Set padding for the button
        getStartedButton.setLayoutX(100); // Set X position of the button
        getStartedButton.setLayoutY(700); // Set Y position of the button
        getStartedButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.jumpToLogin();
            }

        });

        getStartedButton.setOnMouseEntered(e -> getStartedButton.setEffect(shadow));
        getStartedButton.setOnMouseExited(e -> getStartedButton.setEffect(null));

        root.getChildren().add(getStartedButton); // Add button to the root group

        // Image
        Image img = new Image("file:swk\\src\\main\\resources\\Images\\welcomepage\\volunteering.png");
        ImageView imageView = new ImageView(img);
        imageView.setFitWidth(680); // Adjust width of the image
        imageView.setFitHeight(680); // Adjust height of the image
        imageView.setX(1150); // Set X position of the image
        imageView.setY(200); // Set Y position of the image
        root.getChildren().add(imageView); // Add image to the root group

        // Second Image
        Image img2 = new Image("file:swk\\src\\main\\resources\\Images\\logo\\logo1.png");
        ImageView imageView2 = new ImageView(img2);
        imageView2.setLayoutX(0); // Set X position of the image
        imageView2.setLayoutY(-70); // Set Y position of the image
        imageView2.setFitWidth(300); // Adjust width of the image
        imageView2.setFitHeight(300); // Adjust height of the image
        root.getChildren().add(imageView2); // Add image to the root group

        // Create buttons
        HBox hbox = new HBox(10); // Create HBox with spacing between buttons
        hbox.setPadding(new Insets(25)); // Set padding around the HBox
        hbox.setAlignment(Pos.TOP_RIGHT); // Align HBox to the top right corner

        // Create three buttons
        Button homeButton = new Button("Home");
        homeButton.setStyle("-fx-background-color: #F8A404; -fx-text-fill: white; -fx-font-size: 24px; -fx-background-radius: 20px;");
        homeButton.setPadding(new Insets(10, 20, 10, 20)); // Set padding for the button
        homeButton.setOnMouseEntered(e -> homeButton.setEffect(shadow));
        homeButton.setOnMouseExited(e -> homeButton.setEffect(null));

        Button AboutButton = new Button("About us");
        AboutButton.setStyle("-fx-background-color: #F8A404; -fx-text-fill: white; -fx-font-size: 24px; -fx-background-radius: 20px;");
        AboutButton.setPadding(new Insets(10, 20, 10, 20)); // Set padding for the button
        AboutButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.jumpToAboutUs();
            }

        });
        AboutButton.setOnMouseEntered(e -> AboutButton.setEffect(shadow));
        AboutButton.setOnMouseExited(e -> AboutButton.setEffect(null));

        Button loginButton = new Button("Log In");
        loginButton.setStyle("-fx-background-color: #F8A404; -fx-text-fill: white; -fx-font-size: 24px; -fx-background-radius: 20px;");
        loginButton.setPadding(new Insets(10, 20, 10, 20)); // Set padding for the button

        loginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.jumpToLogin();
            }

        });
        loginButton.setOnMouseEntered(e -> loginButton.setEffect(shadow));
        loginButton.setOnMouseExited(e -> loginButton.setEffect(null));

        // Add buttons to the HBox
        hbox.getChildren().addAll(homeButton, AboutButton, loginButton);

        // Position the HBox in the top right corner
        hbox.setLayoutX(1300); // Set X position of the HBox
        hbox.setLayoutY(20); // Set Y position of the HBox

        // Add HBox to the root group
        root.getChildren().add(hbox);
    }

    // Method to get the root group
    public Group getView() {
        return root;
    }
}
