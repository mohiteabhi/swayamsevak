package com.swk.DashBoard;

import com.swk.controller.navigationApp;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

public class Swk_AboutUs {

    private navigationApp app;
    private StackPane view;

    public Swk_AboutUs(navigationApp app) {
        this.app = app;
        initialize();
    }

    private void initialize() {
        Image img = new Image("file:swk\\src\\main\\resources\\Images\\bg4.jpg");

        // Main GridPane
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(20));
        gridPane.setVgap(10);
        gridPane.setAlignment(Pos.TOP_CENTER);

        // Set the background image
        BackgroundImage backgroundImage = new BackgroundImage(
            img,
            BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT,
            BackgroundPosition.DEFAULT,
            BackgroundSize.DEFAULT
        );
        Background background = new Background(backgroundImage);
        gridPane.setBackground(background);

        // Set the GridPane to fill the entire screen
        ColumnConstraints columnConstraints = new ColumnConstraints();
        columnConstraints.setHgrow(Priority.ALWAYS);
        gridPane.getColumnConstraints().add(columnConstraints);

        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setVgrow(Priority.ALWAYS);
        gridPane.getRowConstraints().addAll(rowConstraints, rowConstraints, rowConstraints, rowConstraints);

        // HBox1: Main logo, About Us title, and Home button
        HBox hBox1 = new HBox(20);
        hBox1.setPadding(new Insets(0, 0, 5, 0));  // Adding some padding to the bottom
        hBox1.setAlignment(Pos.CENTER);

        ImageView mainLogo = new ImageView(new Image("file:swk\\src\\main\\resources\\Images\\logo\\logo1.png"));
        mainLogo.setFitHeight(200);
        mainLogo.setPreserveRatio(true);

        Label aboutUsTitle = new Label("About Us");
        aboutUsTitle.setStyle("-fx-font-size: 40px; -fx-font-weight: bold; -fx-text-fill: #434608;");

        Button homeButton = new Button("Home");
        homeButton.setPrefSize(150, 40); // Adjust button size
        homeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                app.jumpToWelcome();
            }
        });

        Region spacerLeft = new Region();
        HBox.setHgrow(spacerLeft, Priority.ALWAYS);

        Region spacerRight = new Region();
        HBox.setHgrow(spacerRight, Priority.ALWAYS);

        hBox1.getChildren().addAll(mainLogo, spacerLeft, aboutUsTitle, spacerRight, homeButton);

        // VBox1: About Us text
        VBox vBox1 = new VBox(0);
        vBox1.setAlignment(Pos.CENTER);

        Label aboutUsText = new Label("At Swayamsevak, our mission is to bridge the gap between enthusiastic volunteers and inspiring events. "
                + "Whether you're an event organizer looking for dedicated volunteers or someone eager to make a difference by participating "
                + "in meaningful events, we've got you covered. Whether you're here to find volunteers or to lend a helping hand, "
                + "Volunteer Finder is your go-to platform. Together, we can make every event a success and every volunteering experience "
                + "rewarding.\n\t\t\t\t\t\t\tThank you for being a part of our community!");
        aboutUsText.setStyle("-fx-font-size: 28px; -fx-text-fill: #505308;"); // Increased font size to 28px
        aboutUsText.setWrapText(true);
        aboutUsText.setMaxWidth(1300);
        aboutUsText.setAlignment(Pos.TOP_CENTER);

        vBox1.getChildren().addAll(aboutUsText);

        // VBox2: "Who are we" label and HBox2 for profiles
        VBox vBox2 = new VBox(20);
        vBox2.setAlignment(Pos.CENTER);

        Label whoAreWeLabel = new Label("     Who are we");
        whoAreWeLabel.setStyle("-fx-font-size: 40px; -fx-font-weight: bold; -fx-text-fill: #434608;");

        HBox hBox2 = new HBox(40);
        hBox2.setAlignment(Pos.CENTER);

        VBox member1 = createTeamMember("Harsh Sanjay Kshirsagar", "file:swk\\src\\main\\resources\\Images\\king1.jpg");
        VBox member2 = createTeamMember("Abhijeet Ramdas Mohite", "file:swk\\src\\main\\resources\\Images\\king2.jpg");
        // VBox member3 = createTeamMember("Shantanu Sonune", "file:/mnt/data/image.png", "https://www.linkedin.com/in/shantanu-sonune");

        hBox2.getChildren().addAll(member1, member2);

        vBox2.getChildren().addAll(whoAreWeLabel, hBox2);

        // VBox3: "Developed Under the Guidance of" label and logo
        HBox hBox3 = new HBox(20);
        hBox3.setAlignment(Pos.CENTER);

        Label guidanceLabel = new Label("Developed Under the Guidance of");
        guidanceLabel.setStyle("-fx-font-size: 30px; -fx-font-weight: bold; -fx-text-fill: white;");

        Button guidanceButton = new Button();
        ImageView guidanceLogo = new ImageView(new Image("file:swk\\src\\main\\resources\\Images\\C2W.png"));
        guidanceLogo.setFitHeight(100);
        guidanceLogo.setPreserveRatio(true);
        guidanceButton.setGraphic(guidanceLogo);
        guidanceButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                app.getHostServices().showDocument("https://www.core2web.in/about-us");
            }
        });

        hBox3.getChildren().addAll(guidanceLabel, guidanceButton);


        // Adding components to GridPane
        gridPane.add(hBox1, 0, 0);
        gridPane.add(vBox1, 0, 1);
        gridPane.add(vBox2, 0, 2);
        gridPane.add(hBox3, 0, 3);

        // Create a VBox to arrange the fields and buttons vertically
        VBox vbox = new VBox(10);  // 10 is the spacing between elements
        vbox.setAlignment(Pos.CENTER);
        vbox.getChildren().addAll(gridPane);

        // Create a StackPane and add the VBox to it
        view = new StackPane();
        view.getChildren().add(vbox);
        // Bind the GridPane size to the StackPane size
        gridPane.prefWidthProperty().bind(view.widthProperty());
        gridPane.prefHeightProperty().bind(view.heightProperty());
    }

    private VBox createTeamMember(String name, String imagePath) {
        VBox member = new VBox(10);
        member.setAlignment(Pos.CENTER);

        ImageView profilePic = new ImageView(new Image(imagePath));
        profilePic.setFitHeight(200);
        profilePic.setFitWidth(200);
        profilePic.setPreserveRatio(true);

        Label nameLabel = new Label(name);
        member.getChildren().addAll(profilePic, nameLabel);
        return member;
    }

    public StackPane getView() {
        return view;
    }
}