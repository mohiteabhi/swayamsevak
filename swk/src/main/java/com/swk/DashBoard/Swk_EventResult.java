package com.swk.DashBoard;

import java.util.List;

import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.swk.controller.navigationApp;
import com.swk.firebase_connection.DataService;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

public class Swk_EventResult {
    


    public Scene scene;  

    public VBox createEventVBox() {
        VBox vb = new VBox();
        vb.setPadding(new Insets(10));
        vb.setSpacing(10);

        // Add your components to the VBox
        Label label = new Label("This is a new VBox from another file.");
        vb.getChildren().add(label);

        return vb;
    }
}

   