package com.swk.controller;

import java.io.FileInputStream;
import java.io.IOException;


import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.swk.firebase_connection.FirebaseService;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LoginController extends Application{

    private Stage primaryStage;
    private navigationApp app; 
    private FirebaseService firebaseService; 
    private Scene scene; 

    public void setPrimaryStageScene(Scene scene){
        primaryStage.setScene(scene);
    }
    public void initializationLoginScene(){
        Scene loginScene = createLoginScene(); 
        this.scene = loginScene; 
        primaryStage.setScene(loginScene);
    }

    @Override
    public void start(Stage primaryStage){
       this.primaryStage = primaryStage; 

       try{
            FileInputStream serviceAccount = new FileInputStream("swk\\src\\main\\resources\\swayamsevak-auth-firebase.json");

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://fx-firebase-ba324-default-rtdb.asia-southeast1.firebasedatabase.app")
                    .build();

            FirebaseApp.initializeApp(options);

       }catch(IOException e){
            e.printStackTrace();
       }

       Scene scene = createLoginScene(); 
       this.scene = scene; 
       primaryStage.setTitle("Firebase Auth Example");
       primaryStage.setScene(scene);
       primaryStage.show();

    }

    private Scene createLoginScene(){
        // Load the logo image
        Image swk_logoImage = new Image("file:swk\\src\\main\\resources\\Images\\i1.png");
        ImageView swk_logoImageView = new ImageView(swk_logoImage);
        swk_logoImageView.setFitWidth(100);
        swk_logoImageView.setFitHeight(100);

        VBox swk_mainLoginLayout = new VBox(15);
        swk_mainLoginLayout.setPadding(new Insets(30));
        swk_mainLoginLayout.setAlignment(Pos.CENTER);
        swk_mainLoginLayout.setStyle("-fx-background-color: #D9D9D9; -fx-border-radius: 10; -fx-background-radius: 20;");

        Text swk_welcomeText = new Text("Hey\nWelcome Back");
        swk_welcomeText.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        swk_welcomeText.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);

        VBox swk_emailLayout = new VBox(5);
        swk_emailLayout.setAlignment(Pos.CENTER_LEFT);
        Label swk_emailLabel = new Label("Email");
        swk_emailLabel.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        TextField swk_emailField = new TextField();
        swk_emailField.setMinHeight(40);
        swk_emailLayout.getChildren().addAll(swk_emailLabel, swk_emailField);

        VBox swk_passwordLayout = new VBox(5);
        swk_passwordLayout.setAlignment(Pos.CENTER_LEFT);
        Label swk_passwordLabel = new Label("Password");
        swk_passwordLabel.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        PasswordField passwordField = new PasswordField();
        passwordField.setMinHeight(40);
        swk_passwordLayout.getChildren().addAll(swk_passwordLabel, passwordField);

        Button signUpButton = new Button("New Member ? SignUp here");
        signUpButton.setStyle("-fx-background-color: transparent; -fx-text-fill: blue; -fx-padding: 0;"); 
        // Login Button
        Button swk_loginButton = new Button("LOGIN");
        swk_loginButton.setFont(Font.font("Arial", FontWeight.BOLD, 18));
        swk_loginButton.setStyle("-fx-background-color: #228B22; -fx-text-fill: white; -fx-border-radius: 5; -fx-background-radius: 5;");
        swk_loginButton.setMinHeight(40);
        swk_loginButton.setMinWidth(100);

        // firebaseService = new FirebaseService(this, swk_emailField, passwordField); 

        signUpButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               firebaseService.signUp();
            }
            
        });
        
        swk_loginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                firebaseService.login();
                app.jumpToAfterLoginPage(); 
            }
            
        });

        // Create a VBox to arrange the fields and button vertically
         swk_mainLoginLayout.getChildren().addAll(swk_logoImageView, swk_welcomeText, swk_emailLayout, swk_passwordLayout, swk_loginButton,signUpButton);
         // Create a StackPane and add the VBox to it
         StackPane view = new StackPane();
         view.getChildren().add(swk_mainLoginLayout);
        return new Scene(view,400,200); 
        
    }
    
}
