package com.swk.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.SetOptions;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firestore.v1.Document;

public class DataService {

    private static Firestore db; 

    static{
        try{
            initializeFirebase(); 
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    private static void initializeFirebase()throws IOException{
        FileInputStream serviceAccount = new FileInputStream(
                "file:swk\\src\\main\\resources\\swayamsevak-7eb39-firebase-adminsdk-h0pki-ff02c5a97a.json");

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://swayamsevak-7eb39-default-rtdb.asia-southeast1.firebasedatabase.app")
                .build();

        FirebaseApp.initializeApp(options);
        db = FirestoreClient.getFirestore(); 
 
    }

    public void addData(String collection, String document, Map<String,Object>data)throws ExecutionException, InterruptedException{
        DocumentReference docRef = db.collection(collection).document(document);
        ApiFuture<WriteResult>result = docRef.set(data); 
        result.get(); 
    }

    public DocumentSnapshot getData(String collection,String document)throws ExecutionException, InterruptedException{
        try{
            DocumentReference docRef = db.collection(collection).document(document);
            ApiFuture<DocumentSnapshot>future = docRef.get(); 
            return future.get(); 
        }catch(Exception e){
            e.printStackTrace();
            throw e; 
        }
    }

    public boolean authenticateUser(String username, String password) throws ExecutionException, InterruptedException{
        DocumentSnapshot document = db.collection("users").document(username).get().get(); 
        if(document.exists()){
            String storePassword = document.getString("password"); 
            return password.equals(storePassword); 
        }
        return false; 
    }

    public boolean isAdmin(String username)throws ExecutionException,InterruptedException{
        DocumentSnapshot document = db.collection("users").document(username).get().get();
        if(document.exists()){
            String role = document.getString("role"); 
            System.out.println(role);
            return "admin".equals(role); 
        }
        return false; 
    }

    public void deleteProject(String collectionName, String leaderName)throws ExecutionException, InterruptedException{
        System.out.println(leaderName);
        ApiFuture<WriteResult> writeResult = db.collection(collectionName).document(leaderName).delete();
        writeResult.get(); 
    }

    public List<Map<String,Object>> getDataInDescendingOrder(String collectionName, String orderByField)throws ExecutionException,InterruptedException{
        List<Map<String, Object>>documenList = new ArrayList<>();

        CollectionReference collection = db.collection(collectionName); 
        Query query = collection.orderBy(orderByField, Query.Direction.DESCENDING); 

        ApiFuture<QuerySnapshot> querySnapshot = query.get(); 

        for(DocumentSnapshot document : querySnapshot.get().getDocuments()){
            documenList.add(document.getData()); 
        }
        return documenList; 
        
    }
    public void updateData(String collectionName, String documentId, Map<String,Object>updatedData)
    throws ExecutionException, InterruptedException{
        CollectionReference collection = db.collection(collectionName); 

        DocumentReference docRef = collection.document(documentId); 
        ApiFuture<WriteResult>future = docRef.set(updatedData, SetOptions.merge()); 

        future.get(); 
    }

}
