package com.swk.controller;

import com.swk.DashBoard.Swk_AboutUs;
import com.swk.DashBoard.Swk_AddEvent;
import com.swk.DashBoard.Swk_AfterLoginPage;
import com.swk.DashBoard.Swk_LoginPage;
import com.swk.DashBoard.Swk_SignUpPage;
import com.swk.DashBoard.Swk_AddEvent;
import com.swk.DashBoard.welcomepage;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class navigationApp extends Application {

    private static navigationApp instance;

    private Stage primaryStage; 
    private Scene page1Scene,page2Scene,page3Scene, page4Scene, page5Scene, page6Scene ; 

    private welcomepage page1; 
    private Swk_LoginPage page2; 
    private Swk_SignUpPage page3; 
    private Swk_AboutUs page4;
    private Swk_AddEvent page5; 
    private Swk_AfterLoginPage page6; 


    @Override
    public void start(Stage primaryStage) throws Exception {

        instance = this; // Initialize the static reference
        System.out.println("navigationApp instance initialized: " + instance);

        this.primaryStage = primaryStage; 



        page1 = new welcomepage(this);
        page2 = new Swk_LoginPage(this); 
        page3 = new Swk_SignUpPage(this);
        page4 = new Swk_AboutUs(this);
        page5 = new Swk_AddEvent(this); 
        page6 = new Swk_AfterLoginPage(this);  

        

       
       
        page1Scene = new Scene(page1.getView(),1920,1080, Color.LIGHTBLUE);
        page2Scene= new Scene(page2.getView(),1920,1080, Color.LIGHTBLUE); 
        page3Scene = new Scene(page3.getView(),1920,1080, Color.LIGHTBLUE); 
        page4Scene = new Scene(page4.getView(),1920,1080, Color.LIGHTBLUE);
        page5Scene = new Scene(page5.getView(),1920,1080, Color.LIGHTBLUE); 
        page6Scene = new Scene(page6.getView(),1920,1080, Color.LIGHTBLUE); 
        



        Image icon = new Image("file:swayamsevak\\swk\\src\\main\\resources\\Images\\welcomepage\\volunteering.png");
        primaryStage.setScene(page1Scene);
        primaryStage.setTitle("स्वयंसेवक");
        primaryStage.getIcons().add(icon);
        // primaryStage.setResizable(false);
        primaryStage.show();

    }
    private void centerStage() {
        primaryStage.centerOnScreen();
    }

    public void jumpToLogin(){
        primaryStage.setScene(page2Scene);
        primaryStage.centerOnScreen();
    }
    public void jumpToWelcome(){
        primaryStage.setScene(page1Scene);
        primaryStage.centerOnScreen();
    }

    public void jumpToSignup(){
        primaryStage.setScene(page3Scene);
        primaryStage.centerOnScreen();
    }

    public void jumpToAboutUs(){
        primaryStage.setScene(page4Scene);
        primaryStage.centerOnScreen();
    }

    public void jumpToAddEvent(){
        primaryStage.setScene(page5Scene);
        primaryStage.centerOnScreen();
    }

    public void jumpToAfterLoginPage(){
        primaryStage.setScene(page6Scene);
        primaryStage.centerOnScreen();
    }

    public void setScene(Scene scene){
        primaryStage.setScene(scene);
    }

    public static navigationApp getInstance() {
        System.out.println("navigationApp.getInstance() called. Instance is: " + instance);
        return instance;
    }
    
}
